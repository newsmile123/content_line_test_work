import moment from "moment";

export const isNumber = (checkedValue, options) => { // проверка на число
  if (isNaN(checkedValue) || checkedValue === null) {
    throw new Error(
      JSON.stringify({
        typeError: `VALUE__NOT_VALID_VALUE`,
        meta: {
          message: "value is not a number",
          valueType: typeof checkedValue,
          ...options
        }
      })
    );
  } else {
    return true;
  }
};

export const reduceData = (reducer, array, defaultObj) => { //просто обертка для редьюса, проверяет входные данные, если все ок производит вызывает reducer функицю иначе кидает ошибку
  const hasArguments = Boolean(array && reducer);
  const isArray = Array.isArray(array);
  const isReducer = typeof reducer === "function";
  if (!hasArguments) {
    throw new Error(
      JSON.stringify({
        typeError: "FUNCTION__NOT_ENOUGH_ARGUMENTS",
        meta: {
          message: `${reduceData.name} | function need argument: ${
            array ? "" : "array"
          } ${reducer ? "" : "reducer"}`
        }
      })
    );
  }
  if (!isArray) {
    throw new Error(
      JSON.stringify({
        typeError: "VALUE__IS_NOT_ARRAY",
        meta: {
          message: `${
            reduceData.name
          } | array must be array, not a ${typeof array}`
        }
      })
    );
  }
  if (!isReducer) {
    throw new Error(
      JSON.stringify({
        typeError: "FUNCTION__REDUCER_IS_NOT_A_FUNCTION",
        meta: {
          message: `${
            reduceData.name
          } | reducer must be function, not a ${typeof array}`
        }
      })
    );
  }

  return array.reduce(reducer, defaultObj);
};

export const convertDateForInput = value =>
  moment(value, "DD.MM.YYYY").format("YYYY-MM-DD"); //форматирование даты формата "DD.MM.YYYY" в "YYYY-MM-DD" так как только с этим форматом работает нативный инпут
export const convertDateForShow = value =>
  moment(value, "YYYY-MM-DD").format("DD.MM.YYYY"); // обратная операция
export const defaultDate = () => moment(new Date()).format("YYYY-MM-DD"); // создает текущую дату и форматирует в "YYYY-MM-DD"

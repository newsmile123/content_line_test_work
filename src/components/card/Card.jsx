import React from "react";
import Card from "react-bootstrap/Card";
import { card, counterBox, counterText } from "./Card.module.scss";

function CardWrapper({ title, counter, content }) {
  return (
    <Card className={card}>
      <Card.Body>
        {title && <Card.Title>{title}</Card.Title>}
        {content && content}
        {counter && (
          <div className={counterBox}>
            <p className={counterText}>{counter}</p>
          </div>
        )}
      </Card.Body>
    </Card>
  );
}

export { CardWrapper };

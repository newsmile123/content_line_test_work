import React from "react";
import { content } from "./ContentBox.module.scss";
function ContentBox({ children, props }) {
  return <article className={content}>{children}</article>;
}

export { ContentBox };

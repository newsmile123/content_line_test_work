import React from "react";
import Spinner from "react-bootstrap/Spinner";
import { container, spinner } from "./SpinnerWrapper.module.scss";



function SpinnerWrapper(props) {
  return (
    <div className={`${container}`}>
      <Spinner className={`${spinner}`} animation="border" variant="primary" />
    </div>
  );
}

export { SpinnerWrapper };

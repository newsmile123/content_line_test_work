import React from "react";
import { stack, column, row } from "./Stack.module.scss";

function Stack({
  children,
  space,
  isColumn = false,
  isRow = false,
  isList = false,
  ...props
}) {
  if (isList) {
    return (
      <ol
        className={`${stack} ${isColumn && column} ${isRow && row}`}
        style={{ "--space": `${space || "2rem"}` }}
      >
        {children}
      </ol>
    );
  }

  if (!isList) {
    return (
      <div
        className={`${stack} ${isColumn && column} ${isRow && row}`}
        style={{ "--space": `${space || "2rem"}` }}
      >
        {children}
      </div>
    );
  }
}

export {Stack};

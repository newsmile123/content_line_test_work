import React from "react";
import { Navbar } from "react-bootstrap";
import { header } from "./Header.module.scss";

function Header({ title, name }) {
  return (
    <Navbar className={header} bg="light">
      <Navbar.Brand href="#home">{name}</Navbar.Brand>
      <Navbar.Text href="#home">{title}</Navbar.Text>
    </Navbar>
  );
}

export { Header };

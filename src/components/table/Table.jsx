import React from "react";
import Table from "react-bootstrap/Table";
import {
  table,
  container,
  header,
  body,
  row,
  row__header,
  col
} from "./Table.module.scss";

function TableWrapper({ data, columns }) {
  return (
    <div className={`${container}`}>
      <Table striped bordered hover className={`${table} `}>
        <thead className={`${header}`}>
          <tr className={`${row}`}>
            {columns.map(column =>
              column.render({
                classNames: { col, row__header },
                isHeader: true,
                currentColumnData: [column.label]
              })
            )}
          </tr>
        </thead>
        <tbody className={`${body}`}>
          {data.map(item => (
            <tr className={`${row}`}>
              {columns.map(column =>
                column.render({
                  classNames: { col, row__header },
                  isData: true,
                  rowData: item,
                  currentColumnData: item[column.key]
                })
              )}
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
}

export { TableWrapper };

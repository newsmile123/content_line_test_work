import React from "react";
import { Bar } from "react-chartjs-2";
import { bar, container } from "./Bar.module.scss";

function BarWrapper({ height, dataset, control, isDateSelected }) {
  return (
    <div>
      {control && control}
      {isDateSelected ? (
        <div className={`${bar} ${container}`} style={{ "--height": height }}>
          <Bar
            data={dataset}
            width={100}
            height={30}
            options={{
              maintainAspectRatio: false
            }}
          />
        </div>
      ) : (
        "Пожалуйста, выберите дату"
      )}
    </div>
  );
}

export { BarWrapper };

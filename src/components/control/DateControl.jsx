import React from "react";
import { container, input, label } from "./DateControl.module.scss";

const DateControl = (value, handleSelectDate) => (
  <div className={container || ""}>
    <label className={input || ""} htmlFor="selectDate">
      Выберети дату:
    </label>
    <input
      className={label || ""}
      type="date"
      id="selectDate"
      name="selectDate"
      value={value}
      onChange={handleSelectDate}
    />
  </div>
);

export { DateControl };

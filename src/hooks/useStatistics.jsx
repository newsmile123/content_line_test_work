import React, { useEffect, useState } from "react";
import {
  convertDateForInput,
  convertDateForShow,
  defaultDate,
  isNumber,
  reduceData
} from "../utils";
import { DateControl } from "../components";
const columns = ({ handleChangeInputValue, representData }) => [
  {
    key: "date",
    label: "Дата",
    flexGrow: "1",
    render: function({
      classNames,
      isHeader = false,
      isData = false,
      currentColumnData
    }) {
      const that = this;
      if (isHeader) {
        return (
          <th
            className={classNames.col || ""}
            style={{ "--flex-grow": that.flexGrow }}
          >
            {currentColumnData}
          </th>
        );
      }
      if (isData) {
        return (
          <td
            className={classNames.col || ""}
            style={{ "--flex-grow": that.flexGrow }}
          >
            <label className={classNames.row__header || ""} htmlFor="td">
              {that.label || ""}
            </label>
            {currentColumnData}
          </td>
        );
      }
      return null;
    }
  },
  {
    key: "name",
    label: "Имя",
    flexGrow: "1",
    render: function({
      classNames,
      isHeader = false,
      isData = false,
      currentColumnData
    }) {
      const that = this;
      if (isHeader) {
        return (
          <th
            className={classNames.col || ""}
            style={{ "--flex-grow": that.flexGrow }}
          >
            {currentColumnData}
          </th>
        );
      }
      if (isData) {
        return (
          <td
            className={classNames.col || ""}
            style={{ "--flex-grow": that.flexGrow }}
          >
            <label className={classNames.row__header || ""} htmlFor="td">
              {that.label || ""}
            </label>
            {currentColumnData}
          </td>
        );
      }
      return null;
    }
  },
  {
    key: "val1",
    label: "val1",
    flexGrow: "2",
    render: function({
      classNames,
      isHeader = false,
      isData = false,
      rowData,
      currentColumnData
    }) {
      const that = this;

      if (isHeader) {
        return (
          <th
            className={classNames.col || ""}
            style={{ "--flex-grow": that.flexGrow }}
          >
            {currentColumnData}
          </th>
        );
      }
      if (isData) {
        return (
          <td
            className={classNames.col || ""}
            style={{ "--flex-grow": that.flexGrow }}
          >
            <label className={classNames.row__header || ""} htmlFor="input">
              {that.label || ""}
            </label>
            <input
              type="text"
              value={currentColumnData}
              onChange={handleChangeInputValue(rowData.name, that.key)}
            />
          </td>
        );
      }
      return null;
    }
  },
  {
    key: "val2",
    label: "val2",
    flexGrow: "2",
    render: function({
      classNames,
      isHeader = false,
      isData = false,
      rowData,
      currentColumnData
    }) {
      const that = this;

      if (isHeader) {
        return (
          <th
            className={classNames.col || ""}
            style={{ "--flex-grow": that.flexGrow }}
          >
            {currentColumnData}
          </th>
        );
      }
      if (isData) {
        return (
          <td
            className={classNames.col || ""}
            style={{ "--flex-grow": that.flexGrow }}
          >
            <label className={classNames.row__header || ""} htmlFor="input">
              {that.label || ""}
            </label>
            <input
              type="text"
              value={currentColumnData}
              onChange={handleChangeInputValue(rowData.name, that.key)}
            />
          </td>
        );
      }

      return null;
    }
  },
  {
    key: "val3",
    label: "val3",
    flexGrow: "2",
    render: function({
      classNames,
      isHeader = false,
      isData = false,
      rowData,
      currentColumnData
    }) {
      const that = this;
      if (isHeader) {
        return (
          <th
            className={classNames.col || ""}
            style={{ "--flex-grow": that.flexGrow }}
          >
            {currentColumnData}
          </th>
        );
      }
      if (isData) {
        return (
          <td
            className={classNames.col || ""}
            style={{ "--flex-grow": that.flexGrow }}
          >
            <label className={classNames.row__header || ""} htmlFor="input">
              {that.label || ""}
            </label>
            <input
              type="text"
              value={currentColumnData}
              onChange={handleChangeInputValue(rowData.name, that.key)}
            />
          </td>
        );
      }
      return null;
    }
  }
];

function delay(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, ms);
  });
}

const fetchStatistics = async () => {
  await delay(300);
  return {
    data: [
      {
        date: "27.08.2017",
        name: "DAY1",
        val1: "10",
        val2: "10",
        val3: "106"
      },
      {
        date: "28.08.2017",
        name: "DAY2",
        val1: "12",
        val2: "20",
        val3: "23"
      },
      { date: "29.08.2017", name: "DAY3", val1: "323", val2: "30", val3: "87" },
      {
        date: "30.08.2017",
        name: "DAY4",
        val1: "43",
        val2: "40",
        val3: "134"
      },
      {
        date: "31.08.2017",
        name: "DAY5",
        val1: "3453",
        val2: "50",
        val3: "55"
      }
    ]
  };
};

const accumulateData = (
  accumulator,
  { name, val1, val2, val3 },
  currentIndex
) => {
  const accumulatorVal1 = accumulator.val1 || 0;
  const accumulatorVal2 = accumulator.val2 || 0;
  const accumulatorVal3 = accumulator.val3 || 0;
  try {
    return {
      ...accumulator,
      val1: isNumber(val1, { ref: "/val1" })
        ? accumulatorVal1 + parseInt(val1)
        : accumulatorVal1,
      val2: isNumber(val2, { ref: "/val2" })
        ? accumulatorVal2 + parseInt(val2)
        : accumulatorVal2,
      val3: isNumber(val3, { ref: "/val3" })
        ? accumulatorVal3 + parseInt(val3)
        : accumulatorVal3
    };
  } catch (e) {
    const {
      meta: { ref: column, message }
    } = JSON.parse(e.message) || {};
    return {
      ...accumulator,
      meta: {
        ...(accumulator.meta || {}),
        errors: [
          ...((accumulator.meta && accumulator.meta.errors) || []),
          {
            name: name,
            ref: `#/${currentIndex}${column}`, //rfc6901 | # текущий объект , currentIndex - строка , column - столбец
            message
          }
        ]
      }
    };
  }
};

const createDataSet = (day, representData) => ({
  labels: [`${console.log(day) || convertDateForShow(day)}`],
  datasets: [
    {
      label: "var1",
      backgroundColor: "rgba(80,255,50,0.2)",
      borderColor: "rgba(80,255,50,1)",
      borderWidth: 1,
      hoverBackgroundColor: "rgba(80,255,50,0.4)",
      hoverBorderColor: "rgba(80,255,50,1)",
      data: [representData["val1"]]
    },
    {
      label: "var2",
      backgroundColor: "rgba(255,99,132,0.2)",
      borderColor: "rgba(255,99,132,1)",
      borderWidth: 1,
      hoverBackgroundColor: "rgba(255,99,132,0.4)",
      hoverBorderColor: "rgba(255,99,132,1)",
      data: [representData["val2"]]
    },
    {
      label: "var3",
      backgroundColor: "rgba(63,65,255,0.2)",
      borderColor: "rgba(63,65,255,1)",
      borderWidth: 1,
      hoverBackgroundColor: "rgba(63,65,255,0.4)",
      hoverBorderColor: "rgba(63,65,255,1)",
      data: [representData["val3"]]
    }
  ]
});

function useStatistics() {
  const [isLoading, setIsLoading] = useState(false);
  const [isDateSelected, setIsDateSelected] = useState(true);
  const [data, setData] = useState([]);
  const [representData, setRepresentData] = useState({});
  const [dataset, setDataset] = useState({});
  const [selectedDate, setDate] = useState(defaultDate());

  useEffect(() => {
    (async () => {
      setIsLoading(true);
      try {
        const response = await fetchStatistics();
        if (response.data.length !== 0) {
          const formatDate = convertDateForInput(response.data[0].date);
          setDate(formatDate);
          setData(response.data);
        }
      } catch (e) {
      } finally {
        setIsLoading(false);
      }
    })();
  }, []);

  useEffect(() => {
    if (data.length === 0) {
    } else {
      setRepresentData(
        reduceData(accumulateData, data, { keys: ["val1", "val2", "val3"] })
      );
    }
  }, [data]);

  useEffect(() => {
    if (data.length === 0 && selectedDate) {
    } else {
      const filteredDataOnSelectedDate = data.filter(
        item => item.date === convertDateForShow(selectedDate)
      );

      const getRepresentDataOnSelectedDate = reduceData(
        accumulateData,
        filteredDataOnSelectedDate,
        {
          keys: ["val1", "val2", "val3"]
        }
      );
      const newDataSet = createDataSet(
        selectedDate,
        getRepresentDataOnSelectedDate
      );

      setDataset(newDataSet);
    }
  }, [data, selectedDate]);

  const handleChangeInputValue = (name, column) => event => {
    const value = event.target.value;

    const newData = data.reduce((accumulator, currentValue) => {
      if (currentValue.name === name) {
        return [
          ...accumulator,
          {
            ...currentValue,
            [column]: value
          }
        ];
      }
      return [...accumulator, currentValue];
    }, []);

    setData(newData);
  };
  const handleSelectDate = e => {
    const value = e.target.value;
    if (value === "") {
      return setIsDateSelected(false);
    }
    setIsDateSelected(true);

    return setDate(value);
  };

  return {
    data,
    representData,
    dataset,
    columns: columns({ handleChangeInputValue, representData }),
    ControlDataset: DateControl(selectedDate, handleSelectDate),
    meta: {
      isDateSelected,
      isLoading
    }
  };
}

export { useStatistics };

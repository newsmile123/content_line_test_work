import React, { cloneElement } from "react";
import { useStatistics } from "./useStatistics";

function StatisticProvider({ children }) {
  const statistics = useStatistics();
  const extendedChildren = cloneElement(children, statistics);
  return <>{extendedChildren}</>;
}

export { StatisticProvider };

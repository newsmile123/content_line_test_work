import React from "react";
import { Card, Stack, Bar, Table, Spinner } from "../components";

function StatisticCard({
  data,
  representData,
  columns,
  dataset,
  ControlDataset,
  meta
}) {
  if (meta.isLoading) {
    return <Spinner />;
  }
  return (
    <Stack isColumn>
      <section>
        <Card
          title={"Общая информация"}
          content={
            <Stack isRow>
              {representData &&
                representData.keys &&
                representData.keys.map(key => (
                  <li key={representData[key]}>
                    <Card title={key} counter={representData[key]} />
                  </li>
                ))}
            </Stack>
          }
        />
      </section>
      <section>
        <Card
          title={"График статистики"}
          content={
              <Bar height={"70vh"} dataset={dataset} control={ControlDataset} isDateSelected={meta.isDateSelected}/>
          }
        />
      </section>
      <section>
        <Card
          title={"Таблица статистики"}
          content={<Table data={data} columns={columns} />}
        />
      </section>
    </Stack>
  );
}

export { StatisticCard };

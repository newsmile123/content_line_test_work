import React from "react";
import { layout } from "./Layout.module.scss";
import { Header, ContentBox } from "../components";
import { StatisticCard } from "../feature";
import { StatisticProvider } from "../hooks";

const titles = {
  header: "Тестовое задание Frontend",
  name: "Влад Шентяпин"
};

function Layout({ className }) {
  return (
    <div className={`${className || ""} ${layout}`}>
      <Header title={titles.header} name={titles.name} />
      <ContentBox>
        <StatisticProvider>
          <StatisticCard />
        </StatisticProvider>
      </ContentBox>
    </div>
  );
}

export { Layout };
